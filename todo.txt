
  - udělat aplikaci konfigurovatelnou z app.init()
    - vytvořit několik základních presetů
      - jen body, body s pár krátkýma linkama, jen dlouhé linky

 - min_connections nefunguje dobře s alphou
   - pokud jeden bod zmizí a další jsou v dostatečné vzdálenosti, tak zmizí najednou bez fadeout animace
     - createLinesFromPoint zkontroluje počet valid_points
     - pokud jich je přesně jako min_connections, tak nastaví všem alpha na nejnižší hodnotu

 - valid_points se nevybírají bezchybně
   - hasConnections počítá se všemi body
   - valid_points nemusí obsahovat body, se kterými hasConnections počítalo
   - rekurzivní fce procházející strom?
     - moc náročné?

 - create makeFnCurryable function in fn.js
   - apply to arePointsInRange, createLine and so on

 - performance test
   - tvoření linek zabere nejvíce času
   - je rychlejší linky každý frame ničit a tvořit nebo updatovat vlastnosti

 - změnit chování bodů a linek na okraji obrazovky
   - v updatePosition změnit flipCoord diameter z max_line_distance na point.diameter
   - propojovat body linkama i přes okraj obrazovky



 - vytvořit nový modul triangle
   - místo linek se budou barvit plochy