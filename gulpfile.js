'use strict'
var fs = require("fs")
var gulp = require('gulp')
var less = require('gulp-less')
var postcss    = require('gulp-postcss')
var sourcemaps = require('gulp-sourcemaps')
var concat = require('gulp-concat')
var addsrc = require('gulp-add-src')
var babel = require("gulp-babel")
var uglify = require("gulp-uglify")

var paths = {
    less: "./css/src/app.less",
    less_src: "./css/src/**/*.less",
    js_app: "./js/src/particles.js",
    js_src: "./js/src/**/*.js",
    js_libs: ["./js/lib/createjs-2015.11.26.min.js"]
}

gulp.task('css_cb', function () {
    var file = "index.html"
    var content = fs.readFileSync(file, 'utf8')
    var hash = '.css?cb=' + Math.round(+new Date()/1000)
    content = content.replace(/\.css\?cb=(\d+)/g, hash)
    return fs.writeFileSync(file, content)
})

var less_task = function(file, output) {
    return gulp.src(file, {base: 'css/'})
        .pipe(concat(output))
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(postcss([require('autoprefixer')]))
        .pipe(sourcemaps.write('.') )
        .pipe(gulp.dest('./css'))
}

gulp.task('less', ["css_cb"], function () {
    return less_task(paths.less, "app.css")
})

gulp.task('js_cb', function () {
    var file = "index.html"
    var content = fs.readFileSync(file, 'utf8')
    var hash = '.js?cb=' + Math.round(+new Date()/1000)
    content = content.replace(/\.js\?cb=(\d+)/g, hash)
    return fs.writeFileSync(file, content)
})

gulp.task('js', ["js_cb"], function () {
    return gulp.src([paths.js_app, paths.js_src], {base: 'js/'})
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(addsrc.prepend(paths.js_libs))
        .pipe(concat("particles.js"))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./js'))
})

gulp.task('js_prod', ["js"], function () {
    return gulp.src([paths.js_app, paths.js_src], {base: 'js/'})
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat("particles.clean.js"))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./js'))
})

gulp.task('js_uglify', ['js_prod'], function() {
    gulp.src('./js/particles.js')
        .pipe(uglify())
        .pipe(gulp.dest('./js'))
    gulp.src('./js/particles.clean.js')
        .pipe(uglify())
        .pipe(gulp.dest('./js'))
});

// Production
gulp.task('prod', ['less', 'js_uglify'])

// Rerun the task when a file changes
gulp.task('watch', function() {
    gulp.watch(paths.less_src, ['less'])
    gulp.watch(paths.js_src, ['js'])
})

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'less', 'js'])