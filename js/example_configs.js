
var config = {

    // WARM ORANGE
    previewWarmOrange: {
        points_per_square: 10,
        point: {
            color: "rgba(255, 255, 255, .33)",
            min_diameter: 1,
            max_diameter: 2.5,
            min_speed: 10,
            max_speed: 20
        },
        line: {
            max_line_distance: 150,
            full_alpha_distance: 50,
            line_color: "rgba(255, 255, 255, .33)"
        }
    },

    // SERIOUS RED
    previewSeriousRed: {
        points_per_square: 5,
        point: {
            color: "rgba(239, 19, 29, .5)",
            min_diameter: .5,
            max_diameter: 2,
            min_speed: 15,
            max_speed: 30
        },
        line: {
            max_line_distance: 200,
            full_alpha_distance: 150,
            line_color: "rgba(239, 19, 29, .25)"
        },
        cursor: {
            max_line_distance: 500,
            full_alpha_distance: 150,
            line_color: "rgba(239, 19, 29, .75)"
        }
    },

    // CREATIVE LINES
    previewCreativeLines: {
        points_per_square: 10,
        point: {
            min_speed: 5,
            max_speed: 20,
            visible: false
        },
        line: {
            max_line_distance: 200,
            full_alpha_distance: 150,
            line_color: "#8F3C43"
        }
    },

    // STANDART
    configDefault: {},

    // MINIMAL
    configMinimal: {
        points_per_square: 1.5,
        point: {
            color: "#999999",
            min_diameter: 1,
            max_diameter: 2,
            min_speed: 5,
            max_speed: 10
        },
        line: {
            max_line_distance: 200,
            full_alpha_distance: 20,
            line_color: "#999999"
        }
    },

    // LINES ONLY & DEBUG
    configLines: {
        points_per_square: 10,
        point: {
            min_speed: 5,
            max_speed: 20,
            visible: false
        },
        line: {
            max_line_distance: 200,
            full_alpha_distance: 150,
            line_color: "#996666"
        }
    },

    // STROKE
    configStroke: {
        points_per_square: 3,
        point: {
            color: "#333",
            min_diameter: 5,
            max_diameter: 5,
            stroke_width: 2,
            stroke_color: "#779",
            min_speed: 20,
            max_speed: 25
        },
        line: {
            max_line_distance: 250,
            full_alpha_distance: 200,
            line_color: "#779",
            line_width: 1
        }
    },

    // WEIRD
    configWeird: {
        fps: .2,
        points_per_square: 5,
        point: {
            color: "#999999",
            min_diameter: 10,
            max_diameter: 20,
            min_speed: 25,
            max_speed: 50
        },
        line: {
            max_line_distance: 200,
            full_alpha_distance: 50,
            line_color: "#220066",
            line_width: 2
        }
    }
}