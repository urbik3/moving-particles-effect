"use strict"

window.particles = window.particles || {}
window.particles.line = window.particles.line || {}

window.particles.line = (function(line, app, createjs) {

    let options = {
        max_line_distance: 200,
        full_alpha_distance: 150,
        line_color: "rgba(150, 150, 150, 1)",
        line_width: 1
    }
    line.options = options

    /*
     * CREATE LINES
     * */
    line.createLinesFromPoint = (createLine, arePointsInRange) => {
        return function(my_point, valid_points) {
            return valid_points
                .filter(point => arePointsInRange(my_point, point))
                .reduce((lines, point) => {
                    let line = createLine(my_point, point)
                    if(line)
                        lines.push(line)
                    return lines
                }, [])
        }
    }

    // line.createLinesFromPoint = (createLine, getDistance, {min_connections, max_line_distance}) => {
    //     return function(my_point, points) {
    //         let lines = [],
    //             valid_points = points
    //                 .filter(point => {
    //                     let distance = getDistance(my_point.shape.x, point.shape.x, my_point.shape.y, point.shape.y)
    //                     return (distance <= max_line_distance) ? (distance !== 0) : false
    //                 })
    //         // console.log(points.length, valid_points.length)
    //
    //         if(valid_points.length >= min_connections) {
    //             valid_points.forEach(point => {
    //                 let line = createLine(my_point, point)
    //                 if(line)
    //                     lines.push(line)
    //             })
    //         }
    //
    //         return lines
    //     }
    // }

    let createLine = (stage, getDistance, lineOptions) => {
        return (point1, point2) => {
            if (point1.lines.find(l => point2.id === l))
                return false

            point1.lines.push(point2.id)
            point2.lines.push(point1.id)

            let line = new createjs.Shape(),
                distance = getDistance(point1.shape.x, point2.shape.x, point1.shape.y, point2.shape.y)

            // GET (CURSOR) LINE OPTIONS
            let cursor = [point1, point2].find(p=>p.isCursor)
            let tempLineOptions = cursor ? Object.assign({}, lineOptions, cursor.cursorOptions) : lineOptions
            let {line_color, line_width, max_line_distance, full_alpha_distance} = tempLineOptions

            // CREATE LINE
            line.alpha = Math.min(1, 1 - (distance - full_alpha_distance) / (max_line_distance - full_alpha_distance))
            line.graphics.setStrokeStyle(line_width).s(line_color).mt(point1.shape.x, point1.shape.y).lt(point2.shape.x, point2.shape.y).endStroke()
            stage.addChild(line)
            stage.setChildIndex(line, 0)
            return line
        }
    }

    /*
    * UTIL
    * */
    line.arePointsInRange = (getDistance, {max_line_distance}) => {
        return (p1, p2) => {
            // CURSOR OPTIONS
            let cursor = [p1, p2].find(p=>p.isCursor)
            max_line_distance = cursor ? cursor.cursorOptions.max_line_distance : max_line_distance
            // GET VALUE
            let distance = getDistance(p1.shape.x, p2.shape.x, p1.shape.y, p2.shape.y)
            return (distance <= max_line_distance) ? (distance !== 0) : false
        }
    }

    // INIT (BAKERY)
    const init = app => {
        options = Object.assign(options, app.options.line)
        
        line.arePointsInRange = line.arePointsInRange(app.getDistance, options)
        createLine = createLine(app.stage, app.getDistance, options)
        line.createLinesFromPoint = line.createLinesFromPoint(createLine, line.arePointsInRange)
    }
    app.addInitCallback(init)

    return line
})(window.particles.line, window.particles, createjs)