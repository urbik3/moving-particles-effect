"use strict"

window.particles = window.particles || {}

window.particles = (function(app, createjs) {

    app.fps = 30
    app.mspf = 1000/app.fps
    app.stage = false
    app.points = []
    app.lines = []

    let options = {
        points_per_square: 5,
        points_square: 100,
        cursor: {},
        debug: false
    }
    app.options = options

    let canvas_parent,
        canvas,
        canvas_width,
        canvas_height

    let initCallbacks = []
    app.addInitCallback = function(fn) {
        initCallbacks.push(fn)
    }

    /*
     * INIT
     * */
    app.init = (canvas_parent_selector, config = {}) => {
        // CONFIGURATE
        if(config.fps) {
            app.fps = config.fps
            app.mspf = 1000/config.fps
        }
        options = Object.assign(options, config)
        if(options.debug)
            console.log("ms per frame:", app.mspf)

        // INIT STAGE
        canvas = document.createElement("canvas")
        canvas_parent = document.querySelector(canvas_parent_selector)
        canvas_parent.appendChild(canvas)

        app.stage = new createjs.Stage(canvas)

        window.addEventListener("resize", app.resize)

        // BAKERY
        initPoints = initPoints(options)
        app.getVirtualCanvasDimensions = app.getVirtualCanvasDimensions(app.line.options)

        // INIT APP
        setCanvasDimensions()
        initCallbacks.forEach(fn=>fn(app))
        setAppVariables()
        app.play()
    }

    app.destroy = () => {
        app.stop()
        app.stage.removeAllEventListeners()
        app.stage.removeAllChildren()
        app.stage = false
        canvas_parent.innerHTML = ""
        window.removeEventListener("resize", app.resize)
    }

    app.stop = () => {
        createjs.Ticker.reset()
    }

    app.play = () => {
        createjs.Ticker.init()
        createjs.Ticker.setFPS(app.fps)
        createjs.Ticker.addEventListener("tick", app.update)
    }

    let initPoints = ({points_square, points_per_square}) => (dimensions) => {
        let num_points_actual = app.points.length,
            num_points = Math.round(Math.sqrt(dimensions.height*dimensions.width)/points_square*points_per_square)

        // REMOVE POINTS
        if(num_points_actual > num_points) {
            app.points
                .splice(0, num_points_actual - num_points)
                .forEach(app.point.remove)
        }
        // ADD POINTS
        else if (num_points_actual < num_points) {

            let createPoints = (num_points, dimensions) => {
                if(num_points <= 0) return []
                num_points--
                return [app.point.createRandomPoint(dimensions), ...createPoints(num_points, dimensions)]
            }

            app.points.push(
                ...createPoints(num_points - num_points_actual, dimensions)
            )
        }
    }

    /*
    * RESIZE STAGE
    * */
    const setCanvasDimensions = () => {
        canvas_height = canvas_parent.clientHeight
        canvas_width = canvas_parent.clientWidth
        canvas.style.height = canvas_height+"px"
        canvas.style.width = canvas_width+"px"
        app.stage.canvas.height = canvas_height
        app.stage.canvas.width = canvas_width
    }

    const setAppVariables = () => {
        app.point.updatePosition = app.point.bakeUpdatePosition(canvas_height, canvas_width)
        initPoints(
            app.getVirtualCanvasDimensions(canvas_width, canvas_height)
        )
    }

    let resizeTimeout
    const resizeToleration = 200
    app.resize = () => {
        if(resizeTimeout)
            clearTimeout(resizeTimeout)

        resizeTimeout = setTimeout(()=>{
            setCanvasDimensions()
            setAppVariables()
            resizeTimeout = false
        }, resizeToleration)
    }

    /*
    * UPDATE
    * */
    app.update = (e) => {
        app.fn.logTime(() => {
            // POSITIONS
            app.points.forEach(p => app.point.updatePosition(p, e, app.stage))

            // LINES
            let lines = [],
                valid_points,
                cursorPoint = options.cursor !== false ? app.point.createPoint(app.stage.mouseX, app.stage.mouseY, options.cursor) : false

            app.points.forEach(p => p.lines = [])

            valid_points = app.fn.array_clone(app.points)
            if(cursorPoint)
                valid_points.unshift(cursorPoint)
            valid_points = valid_points.filter((point, i ,points) => app.point.hasConnections(point, points))

            lines = valid_points.reduce((lines, point, i, points) => {
                let point_lines = app.line.createLinesFromPoint(point, points)
                if(point_lines.length)
                    lines.push(...point_lines)
                return lines
            }, lines)

            app.lines.forEach(line => app.stage.removeChild(line))
            app.lines = lines

            // UPDATE
            app.stage.update()
        }, app.mspf, options.debug ? options.debug : false)
    }

    // UTILS
    app.getDistance = (x1, x2, y1, y2) => (Math.abs(x1 - x2) + Math.abs(y1 - y2))
    app.flipCoord = (coord, bound, diamater = 0) => (coord > bound+diamater) ? -diamater : (coord < -diamater) ? bound+diamater : coord
    app.getPX = (val, delta) => delta/1000*val
    app.getVirtualCanvasDimensions = ({max_line_distance}) =>
        (width, height) => ({
            width: width + (max_line_distance * 2),
            height: height + (max_line_distance * 2)
        })

    return app
})(window.particles, createjs)