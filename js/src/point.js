"use strict"

window.particles = window.particles || {}
window.particles.point = window.particles.point || {}

window.particles.point = (function(point, app, createjs) {

    let options = {
        min_connections: 1,
        color: "rgba(150, 150, 150, 1)",
        min_diameter: 1.5,
        max_diameter: 3,
        stroke_width: 0,
        stroke_color: "transparent",
        min_speed: 15,
        max_speed: 30,
        visible: true
    }

    /*
    * CREATE POINT
    * */
    point.createPoint = ({max_diameter, min_diameter}, drawPoint, getTranslation) => {
        return (x, y, cursorOptions = false) => {
            return drawPoint({
                x,
                y,
                tx: getTranslation(),
                ty: getTranslation(),
                diameter: Math.random() * (max_diameter - min_diameter) + min_diameter,
                isCursor: cursorOptions !== false,
                cursorOptions: cursorOptions === false ? false : Object.assign({}, app.line.options, cursorOptions),
                lines: [],
                id: app.fn.ID()
            })
        }
    }

    point.createRandomPoint =
        ({max_line_distance}) =>
            ({height, width}) =>
                point.createPoint(
                    Math.round(Math.random() * width - max_line_distance),
                    Math.round(Math.random() * height - max_line_distance)
                )

    let drawPoint = (stage, {color, stroke_width, stroke_color, visible}) => {
        return (point) => {
            let shape = new createjs.Shape()
            shape.graphics
                .beginFill(color)
                .setStrokeStyle(stroke_width)
                .s(stroke_color)
                .drawCircle(0, 0, point.diameter)
            shape.x = point.x
            shape.y = point.y

            if(visible && !point.isCursor)
                stage.addChild(shape)
            
            point.shape = shape
            return point
        }
    }

    point.remove =
        (stage) =>
            (point) =>
                stage.removeChild(point.shape)

    /*
    * UPDATE POSITION
    * */
    point.bakeUpdatePosition = ({max_line_distance}) => {
        return (h, w) => {
            return function(point, e) {
                point.shape.x = app.flipCoord(point.shape.x + app.getPX(point.tx, e.delta), w, max_line_distance)
                point.shape.y = app.flipCoord(point.shape.y + app.getPX(point.ty, e.delta), h, max_line_distance)
            }
        }
    }
    point.updatePosition = false

    /*
    * POINT CONNECTIONS
    * */
    point.hasConnections = (arePointsInRange, {min_connections}) => {
        return (my_point, points) => {
            let valid_points = points
                .filter(point => arePointsInRange(my_point, point))

            return valid_points.length >= min_connections
        }
    }

    // UTILS
    let getTranslation = ({min_speed, max_speed}) =>
        () => Math.round(
            (Math.random() > .5 ? 1 : -1) *
            (min_speed +
            Math.random()*(max_speed-min_speed)))

    // INIT (BAKERY)
    const init = app => {
        options = Object.assign(options, app.options.point)
        
        drawPoint = drawPoint(app.stage, options)
        getTranslation = getTranslation(options)
        
        point.createPoint = point.createPoint(options, drawPoint, getTranslation)
        point.createRandomPoint = point.createRandomPoint(app.line.options)
        point.remove = point.remove(app.stage)

        point.bakeUpdatePosition = point.bakeUpdatePosition(app.line.options)
        point.updatePosition = point.bakeUpdatePosition(app.stage.canvas.height, app.stage.canvas.width)

        point.hasConnections = point.hasConnections(app.line.arePointsInRange, options)
    }
    app.addInitCallback(init)

    return point
})(window.particles.point, window.particles, createjs)