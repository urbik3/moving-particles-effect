"use strict"

window.particles = window.particles || {}
window.particles.fn = window.particles.fn || {}

window.particles.fn = (function(fn) {
    fn.array_clone = (arr) =>
        arr.slice(0)

    fn.array_clean = (arr) =>
        arr.filter(Boolean)

    fn.ID = () => '_' + Math.random().toString(36).substr(2, 9)

    fn.logTime = (fn, msTreshold = 0, cb = console.log) => {
        let startTime = new Date().getMilliseconds(),
            endTime,
            time,
            return_val

        return_val = fn()

        endTime = new Date().getMilliseconds()
        time = endTime - startTime
        if (typeof cb === "function")
            cb(time)

        return return_val
    }

    return fn
})(window.particles.fn)